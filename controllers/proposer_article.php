<?php
    require 'upload_photo.php';

    if (isset($_POST['envoyer'])) {
        // On établie la connexion
        $conn = connect_db_pdo();

        $mess = uploadFile($_FILES);
        echo $mess;

    
        // Vérification de la connexion
        if (!$conn) {
            echo "Echec de la connexion : ".mysqli_connect_error();
            exit();
        }
    
        // Date
        $date= date("d.m.y");

        $user = 1;
    
        // On test l'insertion d'article
        try {
            // Ecriture de la requête SQL
            $stmt = $conn->prepare("INSERT INTO articles (titre, contenu, date, photo, ville, id_utilisateur, id_pays, id_continent) 
                    VALUES (:titre, :contenu, :date, :photo, :ville, :id_utilisateur, :id_pays, :id_continent)");        
            $stmt->bindParam(':titre', $_POST['titre']);
            $stmt->bindParam(':contenu', $_POST['contenu']);
            $stmt->bindParam(':date', $date);
            $stmt->bindParam(':photo', $_FILES['photo']['name']);
            $stmt->bindParam(':ville', $_POST['ville']);
            $stmt->bindParam(':id_utilisateur', $user);
            $stmt->bindParam(':id_pays', $_POST['pays']);
            $stmt->bindParam(':id_continent', $_POST['continent']);
            // Execution de la requête
            $stmt->execute();

            // On récupère le dernier identifiant inséré
            $last_id = $conn->lastInsertId();
            // Affichage du message de validation d'exécution
            echo "L'article a bien été ajouté !";
        }
        catch (PDOException $e) {
            echo "Erreur : " . $e->getMessage();
        }

        // Fermeture de la connexion
        $conn = null;
    }

    // On inclue le fichier qui contient la connexion à la base de données
    include("views/proposer_article_views.php");
?>