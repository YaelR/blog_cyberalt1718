<?php

    function uploadFile ($fileUploaded)
    {
        var_dump($fileUploaded);
        $userMessage = "";

        // On test si il y a des données et si il y en a, on fait leur traitement
        if (is_uploaded_file($fileUploaded['photo']['tmp_name'])) {
            echo "truc";
            // Découpage des données du fichier envoyé
            $fichier = $fileUploaded['photo'];
            $nomFichier = $fichier['name'];
            $typeFichier = $fichier['type'];
            $sizeFichier = $fichier['size'];
            $tmp_nameFichier = $fichier['tmp_name'];
            $errorFichier = $fichier['error'];

            // On sépare le nom du fichier et son extension dans deux variables
            $nomExtensionFichier = explode(".", strtolower($nomFichier));
            $nomOnlyFichier = $nomExtensionFichier[0];
            $extensionFichier = $nomExtensionFichier[1];

            $destination = "the_cloud";

            // Contrôles des données

            // Vérification de l'existence d'un nom de fichier identique
            if (file_exists($destination . "/" . $nomFichier)) {
                // Modification du nom pour que le fichier est un nom unique sur le serveur
                // Par la suite le nom de fichier sur le serveur sera unique et on fera un stockage en BDD
                $nomFichier = $nomOnlyFichier . "_" . sha1(uniqid(rand(), true)) . "." . $extensionFichier;
            }

            /*
            * Controle sur le poid du fichier
            * On ne veut pas au delà de 2Mo soit 2^30
            * Lorsque l'on calcul le poid de données, il faut compter en base binaire
            * donc en puissance de 2, et pas en décimal
            * Le décimal est un argument marketing pour simplifier la compréhension de la taille de stockage
            */
            $maxSize = pow(2, 30);

            if ($sizeFichier < $maxSize) {
                /*
                * Extensions autorisées
                * (Le mieux reste de vérifier le type MIME des fichiers uploadés)
                */
                $extensionsAllowed = array('jpg', 'jpeg', 'gif', 'png');

                // Si l'extension est correcte
                if (in_array($extensionFichier, $extensionsAllowed)) {
                // Traitement des codes erreurs (et donc pour l'erreur 0 du traitement du fichier)
                switch ($errorFichier) {
                    case UPLOAD_ERR_OK :
                    // Fichier envoyé

                    // Déplacer le fichier temporaire dans notre dossier de destination
                    // Vérifier que le fichier est bien déplacé
                    if (move_uploaded_file($tmp_nameFichier, $destination . "/" . $nomFichier)) {
                        $userMessage = "Le fichier \"" . $nomFichier . "\" a été transféré.";
                    }
                    else {
                        $userMessage = "Le fichier n'a pas été transféré";
                    }
                    break;

                    case UPLOAD_ERR_NO_FILE :
                    // Pas de fichier saisi
                    $userMessage = "Pas de fichier envoyé";
                    break;

                    case UPLOAD_ERR_INI_SIZE :
                    // Taille de fichier supérieur à uploaded_max_filesize
                    $userMessage = "Taille du fichier trop importante";
                    break;

                    case UPLOAD_ERR_FORM_SIZE :
                    // Taille du fichier supérieur à MAX_FILE_SIZE;
                    $userMessage = "Taille du fichier trop importante";
                    break;

                    case UPLOAD_ERR_PARTIAL :
                    // Fichier partiellement transféré
                    $userMessage = "Le transfert du fichier a été interrompu, veuillez réessayer.";
                    break;

                    case UPLOAD_ERR_NO_TMP_DIR :
                    // Pas de répertoire temporaire
                    $userMessage = "Fichier non transféré";
                    break;

                    case UPLOAD_ERR_CANT_WRITE :
                    // Erreur lors de l'écriture sur le disque
                    $userMessage = "Vous n'avez pas les droits pour écrire sur le disque";
                    break;

                    case UPLOAD_ERR_EXTENSION :
                    // Transfert stoppé par l'extension
                    $userMessage = "Type de fichier non autorisé";
                    break;

                    default :
                    // Erreur que l'on a pas pensé/anticipé
                    $userMessage = "Une erreur est survenue, le fichier n'a pas été transféré.";
                    ecrireDansLesLog("Erreur inconnu à la date sur le formulaire truc : " . $errorFichier);
                    break;
                }
                }
                else {
                $userMessage = "L'extension n'est pas autorisé";
                }
            }
            else {
                $userMessage = "Le fichier ne doit pas dépasser 2Mo";
            }
        }
        else {
        // Envoie d'un message d'erreur pour l'utilisateur
        $userMessage = "Vous devez choisir un fichier.";
        }

        return $userMessage;
    }

?>