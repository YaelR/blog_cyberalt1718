<?php
    // Inserer un utilisateur dans la base de données

    // On inclue le fichier qui contient la connexion à la base de données
    include('../models/connexion_bdd.php');

    // On établie la connexion
    $conn = connect_db_pdo();

    // On test l'insertion de l'utilisateur
    try {
        // Ecriture de la requête SQL
        $sql = "INSERT INTO utilisateur (nom, prenom, mail, mdp, id_role) VALUES ('nom', 'prenom', 'mail', 'mdp', '1')";
        // Execution de la requête
        $conn->exec($sql);
        // On récupère le dernier identifiant inséré
        $last_id = $conn->lastInsertId();
        // Affichage du message de validation d'exécution
        echo "L'utilisateur a bien été ajouté !";
    }
    catch (PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }

    // Fermeture de la connexion
    $conn = null;
?>