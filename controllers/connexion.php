<?php 

$message="";

// On établie la connexion à la BDD
$conn = connect_db_pdo();

if(isset($_POST["connexion"])){
    
    try{
        $req = $conn->prepare('SELECT * FROM utilisateur WHERE mail = :mail');
        $req->execute(array(
            ':mail' => $_POST["mail"]));
        $resultat = $req->fetch();
    
        // Comparaison du pass envoyé via le formulaire avec la base
        $isPasswordCorrect = $_POST['mdp'] === $resultat['mdp'] ? true : false;

        if (!$resultat){
            $message="Mauvais identifiant ou mot de passe !";
        }else {
            if ($isPasswordCorrect) {
                $_SESSION['id_utilisateur'] = $resultat['id_utilisateur'];
                $_SESSION['mail'] = $_POST["mail"];
                $message="Vous êtes connecté";
            } else {
                $message="Problème lors de la connexion";
            }
        }

        header("location: index.php?page=mon_compte");

    } catch(PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }
}

  //INCLUDE VIEWS
  include("views/connexion_views.php");

  ?>