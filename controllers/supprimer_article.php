<?php
    // Supprimer un article dans la base de données

    //// On inclue le fichier qui contient la connexion à la base de données
    include('../models/connexion_bdd.php');

    //On établie la connexion
    $conn = connect_db_pdo();

    // Suppression de l'article
    try {
        // Ecriture de la requête SQL
        $sql = "DELETE FROM articles WHERE id_article=4";
        // Execution de la requête
        $conn->exec($sql);
        echo "L'article a bien été supprimé";
    }
    catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

    // Fermeture de la connexion
    $conn = null;

?>