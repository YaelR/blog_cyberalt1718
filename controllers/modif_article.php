<?php
    // Modifier un article dans la base de données

    // On inclue le fichier qui contient la connexion à la base de données
    include('../models/connexion_bdd.php');

    //On établie la connexion
    $conn = connect_db_pdo();

    // Modification de l'article
    try {
        // Ecriture de la requête SQL
        $sql = "UPDATE articles SET titre='nvtitre', contenu='nvcontenu', photo='nvphoto', ville='nvville' WHERE id_article=1";
        // Préparation de la déclaration (statement)
        $stmt = $conn->prepare($sql);
        // Exécution de la requête déclarée
        $stmt->execute();
        // Affiche le message de validation d'exécution
        echo $stmt->rowCount() . "Article modifié";
    }
    catch (PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }

    // Fermeture de la connexion
    $conn = null;
?>