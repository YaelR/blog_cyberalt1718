<?php

    if (isset($_POST['envoyer'])) {
        // On établie la connexion
        $conn = connect_db_pdo();

        var_dump($_POST);

        // Vérification de la connexion
        if (!$conn) {
            echo "Echec de la connexion : ".mysqli_connect_error();
            exit();
        }
    
        // Date
        $date= date("d.m.y");
    
        // On test l'insertion de formulaires
        try {
            // Ecriture de la requête SQL
            $stmt = $conn->prepare("INSERT INTO formulaires (nom, prenom, date, email, message) 
                    VALUES (:nom, :prenom, :date, :email, :message)");        
            $stmt->bindParam(':nom', $_POST['nom']);
            $stmt->bindParam(':prenom', $_POST['prenom']);
            $stmt->bindParam(':date', $date);
            $stmt->bindParam(':email', $_POST['email']);
            $stmt->bindParam(':message', $_POST['message']);
            // Execution de la requête
            $stmt->execute();

            // Affichage du message de validation d'exécution
            echo "Le formulaire a bien été envoyé !";
        }
        catch (PDOException $e) {
            echo "Erreur : " . $e->getMessage();
        }

        // Fermeture de la connexion
        $conn = null;
    }

    //INCLUDE VIEWS
    include("views/formulaire_contact_views.php");

?>