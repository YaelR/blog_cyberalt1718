<?php
    // On établie la connexion à la BDD
    $conn = connect_db_pdo();

    try {
        // Exécution de la requête SQL
        $results = $conn->query('SELECT id_article, titre, contenu, date, photo, ville, id_utilisateur, id_pays, id_continent FROM articles ORDER BY id_article LIMIT 9 OFFSET 0');
        // Mode de récupération des données
        $results->setFetchMode(PDO::FETCH_OBJ);
    
        // Afficher les données pour chaque ligne retournée
        while ($row = $results->fetch()) {
            echo '<div class="container mtt100 article-home">';
                echo '<a href="?page=' . $row->id_article . '"><h1>' . $row->titre . '</h1></a>';
                echo '<p>Postée le ' . $row->date . '</p>';
                //echo '<p>' . $row->photo . '</p>';
                echo '<div class="row row-articles"><div class="col-lg-6 img-article-home thumbnail"><img src="the_cloud/' . $row->photo . '"/></div></div>';
                //echo '<p>' . $row->id_continent . '</p>';
                //echo '<p><a href="?page=' . $row->id_article . '">En savoir plus</a></p>';
            echo '</div>';
        }
        // Fermeture explicite de la connexion PDO
        $results->closeCursor();
    }
    catch(PDOException $e) {
        echo "Erreur : " . $e->getMessage();
    }

    // Fermeture de la connexion
    $conn = null;

?>
