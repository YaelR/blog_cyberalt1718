<?php

    $lienConnexion="";
    $lienDeconnexion="";
    $lienIsncription="";
    $lienMonCompte="";
    $lienBackOffice="";

    if (isset($_SESSION['id_utilisateur'])) {
        $lienDeconnexion='<a href="?page=logout">Déconnexion</a>';
        $lienMonCompte='<a href="?page=mon_compte">Mon compte</a>';
    } else {
        $lienIsncription='<a href="?page=inscription">Inscription</a>';
        $lienConnexion='<a href="?page=connexion">Connexion</a>';
    }

    //INCLUDE VIEWS
    include("views/header_views.php");

?>