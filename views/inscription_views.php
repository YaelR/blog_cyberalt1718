<div class="container">

    <h1 class="mtt100">Page Inscription</h1>
    <h2>Inscris-toi :)</h2>

    <form action="?page=inscription" method="post">
        <div class="form-group">
            <label for="nom">Nom :</label>
            <input type="text" class="form-control" id="nom" name="nom" placeholder="Entrez votre nom">
        </div>
        <div class="form-group">
            <label for="prenom">Prénom :</label>
            <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Entrez votre prénom">
        </div>
        <div class="form-group">
            <label for="email">Email :</label>
            <input type="email" class="form-control" id="email" name="mail" placeholder="Entrez votre mail">
        </div>
        <div class="form-group">
            <label for="mdp">Mot de passe :</label>
            <input type="password" class="form-control" id="mdp" name="mdp" placeholder="Entrez votre mot de passe">
        </div>
        <div>
            <button type="submit" class="btn" id="btnCheckInscription" name="valider">Valider mon inscription</button>
        </div>
    </form>
</div>