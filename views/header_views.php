<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Blog Trotters</title>

    <!--FAVICON-->
    <link rel="icon" href="favicon.ico" />
    <!--JQUERY-->
	<script src="src/vendor/js/jquery-3.2.1.js"></script>
	<!--BOOTSTRAP-->
	<link rel="stylesheet" href="src/vendor/bootstrap-3.3.7-dist/css/bootstrap.css">
    <script src="src/vendor/bootstrap-3.3.7-dist/js/bootstrap.js" ></script>
    <!--CSS-->
    <link rel="stylesheet" href="src/css/style.css">
    <!--TYPOS-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Shadows+Into+Light" rel="stylesheet"> 
    <!--Font-Awesome-->
    <link rel="stylesheet" href="src/vendor/font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--MENU-->
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <a class="navbar-brand logo-blog" href="?page=home"><img src="src/img/logo_blog_voyage.png" alt="logo blog voyage"></a>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="?page=afficher_articles">Voir les articles</a></li>
                        <li><a href="?page=proposer_article">Proposer un article</a></li>
                        <li><?php echo $lienConnexion ?></li>
                        <li><?php echo $lienDeconnexion ?></li>
                        <li><?php echo $lienIsncription ?></li>
                        <li><?php echo $lienMonCompte ?></li>
                        <li><a href="?page=formulaire_contact">Contactez-nous !</a></li>
                        <li><?php echo $lienBackOffice ?></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
