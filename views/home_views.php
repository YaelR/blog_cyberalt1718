<!--IMG ACCUEIL-->
<div class="slide-accueil">
    <img class="img-responsive" src="src/img/priscilla-du-preez-363211-unsplash.jpg" alt="photo accueil blog voyage">
</div>

 <!--MOTEUR DE RECHERCHE-->
 <div class="container mdr-accueil">
    <h1>Un slogan qui claque sa mère</h1>
    <input type="search" placeholder=" En route pour l'aventure !">
</div>

<!--AFFICHAGE DES ARTICLES-->
<div class="container">
<h2>Les dernières destinations :)</h2>
 
<div class="img-article-home">
    <?php
        // On établie la connexion à la BDD
        $conn = connect_db_pdo();

        try {
            // Exécution de la requête SQL
            $results = $conn->query('SELECT id_article, titre, contenu, date, photo, ville, id_utilisateur, id_pays, id_continent FROM articles ORDER BY id_article LIMIT 9 OFFSET 0');
            // Mode de récupération des données
            $results->setFetchMode(PDO::FETCH_OBJ);
                
            // Afficher les photos pour chaque ligne retournée
            while ($row = $results->fetch()) {
                echo '<div class="grid-padding">';
                    //echo '<a href="?page=' . $row->id_article . '"><h1>' . $row->titre . '</h1></a>';
                    //echo '<p>Postée le ' . $row->date . '</p>';
                    //echo '<p>' . $row->photo . '</p>';
                    echo '<div class="col-xs-12 col-sm-4 img-article-home"><img class="img-responsive" src="the_cloud/' . $row->photo . '"/></div>';
                    //echo '<p>' . $row->id_continent . '</p>';
                    //echo '<p><a href="?page=' . $row->id_article . '">En savoir plus</a></p>';
                echo '</div>';
            }
            // Fermeture explicite de la connexion PDO
            $results->closeCursor();
        }
        catch(PDOException $e) {
            echo "Erreur : " . $e->getMessage();
        }

        // Fermeture de la connexion
        $conn = null;
    ?>
<!-- </div>
    <div class="articles-home img-article-home">

        <div class="row row-articles">
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/instagram-com-jamie_fenn-276420-unsplash.jpg" alt="photo utilisateur">
            </div>
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/jeremie-cremer-4419-unsplash.jpg" alt="photo utilisateur">
            </div>
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/edgar-chaparro-143297-unsplash.jpg" alt="photo utilisateur">
            </div>
        </div>

        <div class="row row-articles">
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/philipp-kammerer-346387-unsplash.jpg" alt="photo utilisateur">
            </div>
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/rebe-adelaida-293711-unsplash.jpg" alt="photo utilisateur">
            </div>
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/david-marcu-114194-unsplash.jpg" alt="photo utilisateur">
            </div>
        </div>

        <div class="row row-articles">
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/jon-tyson-520952-unsplash.jpg" alt="photo utilisateur">
            </div>
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/julien-laurent-374836-unsplash.jpg" alt="photo utilisateur">
            </div>
            <div class="col-xs-12 col-sm-4 img-article-home">
                <img src="src/img/pascal-debrunner-634122-unsplash.jpg" alt="photo utilisateur">
            </div>
        </div> -->
    </div>
 </div>