<div class="container">
<h1 class="mtt100">Contactez_nous : </h1>
<form action="?page=formulaire_contact" method="post">
    <div class="form-group">
        <label for="nom">Nom :</label>
        <input type="text" id="nom" name="nom" class="form-control" placeholder="Entrez votre nom">
    </div>
    <div class="form-group">
        <label for="prenom">Prénom :</label>
        <input type="text" id="prenom" name="prenom" class="form-control" placeholder="Entrez votre prénom">
    </div>
    <div class="form-group">
        <label for="mail">Email :</label>
        <input type="email" id="mail" name="email" class="form-control" placeholder="Entrez votre mail">
    </div>
    <div class="form-group">
        <label for="msg">Message :</label>
        <textarea id="msg" name="message" class="form-control" placeholder="Ecrivez votre message"></textarea>
    </div>
    <button id="btnCheckMessage" type="submit" class="btn" name="envoyer" value="envoyer">Envoyer</button>
</form>
</div>