<div class="container">

    <h1 class="mtt100">Proposer un article</h1>

    <form action="?page=proposer_article" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="titre">Titre :</label>
            <input type="text" class="form-control" id="titre" name="titre" placeholder="Entrez le titre de votre article">
        </div>
        <div class="form-group">
            <label for="contenu">Contenu :</label>
            <textarea cols='60' type="text" class="form-control" id="contenu" name="contenu" placeholder="Entrez une description"></textarea>
        </div>
        <div class="form-group">
            <label for="photo">Photo :</label>
            <input type="file" class="form-control" name="photo" id="photo">
        </div>
        <div class="form-group">
            <label for="continent">Continent :</label>
            <select name="continent">
                <?php
                    // On établit la connexion
                    $conn = connect_db_pdo();

                    // Sélection avec la méthode PDO (Orienté Objet)
                    try {
                        // Exécution de la requête SQL
                        $results = $conn->query("SELECT * FROM continent");
                        // Mode de récupération des données
                        $results->setFetchMode(PDO::FETCH_OBJ);
                        // Affiche les données pour chaque ligne retourné
                        while($row = $results->fetch())
                        {
                            echo "<option value=" . $row->id_continent . ">" . $row->continent . "</option>";
                        }
                        // Fermeture explicite de la connexion PDO
                        $results->closeCursor();
                    }
                    catch(PDOException $e) {
                        echo "Erreur : " . $e->getMessage();
                    }

                    // Fermeture de la connexion
                    $conn = null;
                ?>
            </select>        
        </div>
        <div class="form-group">
            <label for="pays">Pays :</label>
            <select name="pays">
                <?php
                    // On établit la connexion
                    $conn = connect_db_pdo();

                    // Sélection avec la méthode PDO (Orienté Objet)
                    try {
                        // Exécution de la requête SQL
                        $results = $conn->query("SELECT * FROM pays");
                        // Mode de récupération des données
                        $results->setFetchMode(PDO::FETCH_OBJ);
                        // Affiche les données pour chaque ligne retourné
                        while($row = $results->fetch())
                        {
                            echo "<option value=" . $row->id_pays . ">" . $row->pays . "</option>";
                        }
                        // Fermeture explicite de la connexion PDO
                        $results->closeCursor();
                    }
                    catch(PDOException $e) {
                        echo "Erreur : " . $e->getMessage();
                    }

                    // Fermeture de la connexion
                    $conn = null;
                ?>
            </select>
        </div>
        <div id="results"></div>
        <div class="form-group">
            <label for="ville">Ville :</label>
            <input type="text" class="form-control" id="ville" name="ville" placeholder="Entrez la ville où la photo a été prise">
        </div>
        <div>
            <button type="submit" class="btn" id="btnCheckInscription" name="envoyer" value="envoyer">Envoyer mon article</button>
        </div>
    </form>

</div>
