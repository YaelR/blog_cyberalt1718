<div class="container">

    <h1 class="mtt100">Page de connexion</h1>
    <h2>Connecte-toi :)</h2>

    <form action="?page=connexion" method="post">
        <div class="form-group">
            <label for="email">Email :</label>
            <input type="email" class="form-control" id="mail" name="mail" placeholder="Entrez votre mail">
        </div>
        <div class="form-group">
            <label for="mdp">Mot de passe :</label>
            <input type="password" class="form-control" id="mdp" name="mdp" placeholder="Entrez votre mot de passe">
        </div>
        <div>
            <button type="submit" class="btn" id="btnCheckConnexion" name="connexion">Connexion</button>
        </div>
    </form>
    <div><?php echo $message; ?></div>
    </div>

</div>