<?php 
    //DEMARRE LA SESSION
	session_start();

	$_SESSION["adresseSite"]="http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
	
	//TESTE QU EST CE QU IL A DANS  LE TABLEAU $_SESSION
	//var_dump($_SESSION);	


	//FONCTION POUR SE CONNECTER A LA BDD
	include("models/connexion_bdd.php");
	//$cn = connect_db_pdo();

    //AFFICHE LA PAGE D'ACCUEIL
	$page = isset($_GET["page"]) ? $_GET["page"] : "home";

    //SI LA PAGE N EXISE PAS ON ENVOI LA PAGE 404
	if (!file_exists("controllers/".$page.".php")) $page="404";

    //INCLURE LE FICHIER HEADER, LE BODY DE LA PAGE ET LE FOOTER
	include("controllers/header.php");
	include("controllers/".$page.".php");
	include("controllers/footer.php");


 ?>