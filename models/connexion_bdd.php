<?php
    // Connexion à la base de données MySQL : blog_voyage

    // Fonction de connexion à la BDD avec la méthode PDO (Orienté Objet)
    function connect_db_pdo() {
        // Nom de la BDD
        $dbname = 'blog_voyage';
        // Nom du serveur
        $hostname = 'localhost';
        // Utilisateur pour MySQL
        $username = 'root';
        // Mot de passe pour MySQL
        $password = '';

        // Ouverture de la connexion avec la méthode PDO (Orienté Objet)
        // Test de la connexion
        try {
            $conn = new PDO('mysql:host=' . $hostname . ';dbname=' . $dbname, $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // echo "Connexion PDO réussie<br>"; 
        }
        catch(PDOException $e)
        {
            echo "Connexion PDO échouée: " . $e->getMessage() . "<br>";
        }

        // On retourne l'objet de connexion
        return $conn;
    }

?>