Consignes : par groupe de 2/3, vous allez cr�er un blog

- 1 page d'accueil qui affiche les articles
- 1 page pour l'article
- 1 page de contact

Contraintes :
- code propre, indent� et comment�
- utilisation de GitLab
- utiliser du pseudo code dans le processus de d�veloppement

BDD :
- au moins une table "articles"
- les colonnes : "id_article", "titre", "texte", "date"


GitLab :

cd existing_folder
git init
git remote add origin https://gitlab.com/YaelR/blog_cyberalt1718.git
git add .
git commit -m "Initial commit"
git push -u origin master